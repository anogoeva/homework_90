import React, {useState, useRef, useEffect} from 'react';

const Canvas = () => {
    const ws = useRef(null);
    const [state, setState] = useState({
        mouseDown: false,
        pixelsArray: [],
        savedCanvas: []
    });

    useEffect(() => {
        ws.current = new WebSocket('ws://localhost:8000/canvas');
        ws.current.onmessage = event => {
            const decoded = JSON.parse(event.data);

            if (decoded.type === 'NEW_CANVAS') {
                const context = canvas.current.getContext('2d');
                const imageData = context.createImageData(1, 1);
                decoded.pixelsArray.forEach(pixel => {
                    const t = imageData.data;
                    t[0] = 0;
                    t[1] = 0;
                    t[2] = 0;
                    t[3] = 255;
                    context.putImageData(imageData, pixel.x, pixel.y);
                });
            }

            if (decoded.type === 'CONNECTED') {
                const context = canvas.current.getContext('2d');
                const imageData = context.createImageData(1, 1);
                if (decoded.savedCanvas !== null) {
                    decoded.savedCanvas.forEach(pixel => {
                        const t = imageData.data;
                        t[0] = 0;
                        t[1] = 0;
                        t[2] = 0;
                        t[3] = 255;
                        context.putImageData(imageData, pixel.x, pixel.y);
                    });
                }
            }
        };
    }, []);

    const canvas = useRef(null);

    const canvasMouseMoveHandler = event => {
        if (state.mouseDown) {
            const clientX = event.clientX;
            const clientY = event.clientY;
            setState(prevState => {
                return {
                    ...prevState,
                    pixelsArray: [...prevState.pixelsArray, {
                        x: clientX,
                        y: clientY
                    }]
                };
            });

            const context = canvas.current.getContext('2d');
            const imageData = context.createImageData(1, 1);
            const d = imageData.data;

            d[0] = 0;
            d[1] = 0;
            d[2] = 0;
            d[3] = 255;

            context.putImageData(imageData, event.clientX, event.clientY);
        }
    };

    const mouseDownHandler = event => {
        setState({...state, mouseDown: true});
    };

    const mouseUpHandler = event => {
        ws.current.send(JSON.stringify({
            type: 'CREATE_CANVAS',
            pixelsArray: state.pixelsArray
        }));
        setState({...state, mouseDown: false, pixelsArray: []});
    };

    return (
        <div>
            <canvas
                ref={canvas}
                style={{border: '1px solid black'}}
                width={800}
                height={600}
                onMouseDown={mouseDownHandler}
                onMouseUp={mouseUpHandler}
                onMouseMove={canvasMouseMoveHandler}
            />
        </div>
    );
};

export default Canvas;