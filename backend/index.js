const express = require('express');
const cors = require('cors');
const {nanoid} = require('nanoid');
const app = express();

require('express-ws')(app);

const port = 8000;

app.use(cors());

const activeConnections = {};
let savedCanvas = null;
app.ws('/canvas', (ws, req) => {
    const id = nanoid();
    activeConnections[id] = ws;

    console.log(savedCanvas);
    ws.send(JSON.stringify({
        type: 'CONNECTED',
        savedCanvas,
    }));
    ws.on('message', msg => {
        const decoded = JSON.parse(msg);

        switch (decoded.type) {
            case 'CREATE_CANVAS':
                Object.keys(activeConnections).forEach(key => {
                    const connection = activeConnections[key];
                    connection.send(JSON.stringify({
                        type: 'NEW_CANVAS',
                        pixelsArray: decoded.pixelsArray,
                    }));
                    savedCanvas = decoded.pixelsArray;
                    console.log(savedCanvas);
                });
                break;
            default:
                console.log('Unknown type: ', decoded.type);
        }
    });
    ws.on('close', () => {
        console.log(`Client disconnected! id=${id}`);
        delete activeConnections[id];
    });

    console.log(`Client connected with id = ${id}`);
});

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});